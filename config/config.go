package config

import (
	"github.com/spf13/cast"
	"os"
)

type Config struct {
	PsqlUser string
	PsqlPassword string
	PsqlDb string
}

func Load() Config {
	cfg:=Config{}
	cfg.PsqlUser = cast.ToString(getReturnDefault("POSTGRES_USER","user"))
	cfg.PsqlPassword = cast.ToString(getReturnDefault("POSTGRES_PASSWORD","password"))
	cfg.PsqlDb = cast.ToString(getReturnDefault("POSTGRES_DB","articles"))
	return cfg
}

func getReturnDefault(key string, defaultValue interface{}) interface{} {
	val,ok:=os.LookupEnv(key)
	if ok {
		return val
	}
	return defaultValue
}

