package repos

import "bootcamp/article/models"

type ArticleI interface {
	Add(entity models.CreateArticle) error
	GetById(ID int) (models.Article, error)
	GetAll() []models.Article
	Search(str string) []models.Article
	Update(entity models.UpdateArticle) error
	Delete(ID int) error
}



