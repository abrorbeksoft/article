package postgres

import (
	"bootcamp/article/models"
	"bootcamp/article/storage/repos"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
)

type storage struct {
	session *sqlx.DB
}

func NewArticles(session *sqlx.DB) repos.ArticleI {
	return &storage{
		session: session,
	}
}

var ErrorNotFound = errors.New("not found")
var ErrorAlreadyExists = errors.New("already exists")

func (s *storage) Add(entity models.CreateArticle) error {
	res,err:=s.session.Exec("INSERT INTO articles (title,body,author_id,created_at,updated_at) values ($1,$2,$3,now(),now())",entity.Title,entity.Body,entity.AuthorId)
	if err!=nil {
		return err
	}
	log.Println(res.RowsAffected())
	return nil
}

func (s *storage) GetById(ID int) (models.Article, error) {
	var resp models.Article
	err:=s.session.QueryRow("select art.id, art.title, art.body,auth.first_name,auth.last_name,art.created_at,art.updated_at from articles as art inner join authors as auth on art.author_id=auth.id where art.id=$1",ID).Scan(&resp.ID,&resp.Title,&resp.Body,&resp.Author.Firstname,&resp.Author.Lastname,&resp.CreatedAt,&resp.UpdatedAt)
	if err!=nil {
		return models.Article{},err
	}

	return resp, nil
}

func (s *storage) GetAll() []models.Article {
	var resp []models.Article
	rows,_:=s.session.Query("select art.id, art.title, art.body,auth.first_name,auth.last_name,art.created_at,art.updated_at from articles as art inner join authors as auth on art.author_id=auth.id")

	for rows.Next() {
		var article models.Article
		err:=rows.Scan(&article.ID,&article.Title,&article.Body,&article.Author.Firstname,&article.Author.Lastname,&article.CreatedAt,&article.UpdatedAt)
		if err != nil {
			continue
		}
		resp = append(resp,article)
	}
	defer rows.Close()
	return resp
}

func (s *storage) Search(str string) []models.Article {
	var resp []models.Article

	if str=="" || len(str) == 0 {
		return s.GetAll()
	}
	sql:="SELECT art.id, art.title, art.body,auth.first_name,auth.last_name,art.created_at,art.updated_at from articles as art inner join authors as auth on art.author_id=auth.id and (art.title like $1 or art.body like $1 or auth.first_name like $1 or auth.last_name like $1)"
	query, err:=s.session.Query(sql,"%"+str+"%")
	if err!=nil {
		log.Println(err.Error())
	}
	for query.Next() {
		var article models.Article
		err:=query.Scan(&article.ID,&article.Title,&article.Body,&article.Author.Firstname,&article.Author.Lastname,&article.CreatedAt,&article.UpdatedAt)
		if err!=nil {
			log.Println(err.Error())
			continue
		}
		resp = append(resp,article)
	}
	return resp
}

func (s *storage) Update(entity models.UpdateArticle) error {
	resp,err:=s.session.Exec(`update articles set "title"=$1, "body"=$2,updated_at=now() where id=$3`,entity.Title,entity.Body,entity.Id)
	if err!=nil {
		log.Println(err.Error())
		return err
	}
	log.Println(resp.RowsAffected())
	return nil
}

func (s *storage) Delete(ID int) error {
	resp,err:=s.session.Exec(`DELETE FROM articles where id=$1`,ID)
	if err!=nil {
		log.Println(err.Error())
		return err
	}
	fmt.Println(resp.RowsAffected())
	return nil
}