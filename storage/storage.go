package storage

import (
	"bootcamp/article/storage/postgres"
	"bootcamp/article/storage/repos"
	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	Article() repos.ArticleI
}

type postgresStorage struct {
	articleStorage repos.ArticleI
}

func NewStorage(session *sqlx.DB) StorageI {
	return &postgresStorage{
		articleStorage: postgres.NewArticles(session),
	}
}

func (p *postgresStorage) Article() repos.ArticleI {
	return p.articleStorage
}

