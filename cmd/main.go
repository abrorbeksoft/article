package main

import (
	"bootcamp/article/config"
	server "bootcamp/article/server"
	"bootcamp/article/storage"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
)

func main() {
	cfg:=config.Load()
	connStr:=fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",cfg.PsqlUser,cfg.PsqlPassword,cfg.PsqlDb)
	db,err:=sqlx.Open("postgres",connStr)
	if err!=nil {
		log.Fatal(err.Error())
	}
	router:=server.New(storage.NewStorage(db))
	router.Run(":8080")
}
