package models

import "time"

type Content struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}

type Article struct {
	ID        int        `json:"id"`
	Content              // Promoted fields
	Author    Person     `json:"author"` // Nested structs
	CreatedAt *time.Time `json:"-"`
	UpdatedAt *time.Time `json:"_"`
}

type CreateArticle struct {
	Content
	AuthorId int `json:"author_id"`
}

type UpdateArticle struct {
	Id int `json:"id"`
	Content

}

type BadResponse struct {
	Code int `json:"code"`
	Data interface{} `json:"data"`
}

type SuccessResponse struct {
	Code int `json:"code"`
	Data interface{} `json:"data"`
}