package test

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"testing"
	"time"
)

func add(a models.Article) error {
	return inMemory.Add(a)
}

func getByID(ID int) (models.Article, error) {
	return inMemory.GetByID(ID)
}

func count() int {
	return len(inMemory)
}

func TestStorage(t *testing.T) {
	t1 := time.Now()
	a1 := models.Article{
		ID: 1,
		Content: models.Content{
			Title: "Lorem",
			Body:  "Lorem ipsum",
		},
		Author: models.Person{
			Firstname: "John",
			Lastname:  "Doe",
		},
		CreatedAt: &t1,
	}

	//// Positive test case
	c := count()
	err := add(a1)
	if err != nil {
		t.Errorf("Add FAILED: %v", err)
	}
	if count() != c+1 {
		t.Errorf("Add FAILED: expected count %d but got %d", c+1, count())
	}
	////

	//// Negative test case
	err = add(a1)
	if err != storage.ErrorAlreadyExists {
		t.Error("Add FAILED: should return error", storage.ErrorAlreadyExists)
	}
	////

	t.Log("Add PASSED")

	res1, err := getByID(a1.ID)
	if err != nil {
		t.Errorf("GetByID FAILED: %v", err)
	}
	if a1 != res1 {
		t.Errorf("Add or GetByID FAILED: expected %v but got %v", a1, res1)
	}

	_, err = getByID(-1)
	if err != storage.ErrorNotFound {
		t.Errorf("Add returns error: %v", err)
	}

	t.Log("GetByID PASSED")

}
