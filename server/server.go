package server

import (
	_ "bootcamp/article/server/docs"
	"bootcamp/article/server/handlers"
	"bootcamp/article/storage"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"       // swagger embed files
	"github.com/swaggo/gin-swagger" // gin-swagger middleware
	"net/http"
)


// @title Swagger Example API
// @version 1.0
// @description This is a sample server celler server.
// @termsOfService http://swagger.io/terms/
func New(storage storage.StorageI) *gin.Engine {

	gin.SetMode(gin.DebugMode)
	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())
	handler:=handlers.Handler(storage)

	r.GET("/", func(context *gin.Context) {
		context.JSON(http.StatusOK, gin.H{
			"message" : "Server is running",
		})
	})
	routerV1:=r.Group("/v1")

	routerV1.Use()
	{
		routerV1.POST("/articles", handler.CreateHandler)
		routerV1.GET("/articles", handler.GetAllHandler)
		routerV1.GET("/articles/:id", handler.GetByIDHandler)
		routerV1.PUT("/articles/:id", handler.UpdateHandler)
		routerV1.DELETE("/articles/:id", handler.DeleteHandler)
	}
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	return r
}

