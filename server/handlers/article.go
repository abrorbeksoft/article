package handlers

import (
	"bootcamp/article/models"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)


// @Router /v1/articles/{id} [PUT]
// @Tags Articles
// @Summary Update article by its id
// @Description Update article by its id
// @ID update-article-by-its-id
// @Param id path string true "Id of the article"
// @Param article body models.Article true "body"
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Article
// @Failure 400,404 {object} models.BadResponse
func (h *handler) UpdateHandler(c *gin.Context) {
	idStr := c.Param("id")
	var article models.UpdateArticle
	var err error

	err = c.BindJSON(&article)
	if err!= nil {
		h.sendBadResponse(c, err.Error())
		return
	}

	article.Id, _ = strconv.Atoi(idStr)
	err=h.inMemory.Article().Update(article)
	if err!= nil {
		h.sendBadResponse(c, err.Error())
		return
	}
	c.JSON(http.StatusOK, article)
}

// @Router /v1/articles/{id} [DELETE]
// @Tags Articles
// @Summary Delete article by its id
// @Description Delete article by its id
// @ID delete-article-by-its-id
// @Param id path string true "Id of the article"
// @Accept  json
// @Produce  json
// @Success 200 {array} models.SuccessResponse
// @Failure 400,404 {object} models.BadResponse
func (h *handler)  DeleteHandler(c *gin.Context)  {
	idStr:=c.Param("id")
	id, err:=strconv.Atoi(idStr)
	if err!= nil{
		h.sendBadResponse(c, err.Error())
		return
	}

	err=h.inMemory.Article().Delete(id)
	if err!= nil {
		h.sendBadResponse(c, err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message" : "Successfully deleted",
	})
}

// @Router /v1/articles [POST]
// @Tags Articles
// @Summary Create article
// @Description Create article
// @ID create-articles-handler
// @Param article body models.Article true "Article"
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Article
// @Failure 400,404 {object} models.BadResponse
func (h *handler)  CreateHandler(c *gin.Context) {
	var a1 models.CreateArticle
	err := c.BindJSON(&a1)
	if err != nil {
		h.sendBadResponse(c, err.Error())
		return
	}
	err = h.inMemory.Article().Add(a1)
	if err != nil {
		h.sendBadResponse(c, err.Error())
		return
	}
	c.String(200, "Ok")
}

// @Router /v1/articles [GET]
// @Tags Articles
// @Summary Get all articles
// @Description Get all articles
// @ID get-article-handler
// @Param q query string false "q"
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Article
// @Failure 400,404 {object} models.BadResponse
func (h *handler)  GetAllHandler(c *gin.Context) {
	query:=c.Query("q")

	if query!= "" {
		resp:=h.inMemory.Article().Search(query)
		c.JSON(http.StatusOK, resp)
		return
	}

	resp := h.inMemory.Article().GetAll()
	c.JSON(200, resp)
}

// @Router /v1/articles/{id} [GET]
// @Tags Articles
// @Summary Get article by id
// @Description Get article by id
// @ID get-article-by-id-handler
// @Param id path string true "Id of the article"
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Article
// @Failure 400,404 {object} models.BadResponse
func (h *handler)  GetByIDHandler(c *gin.Context) {
	idStr := c.Param("id")
	idNum, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		h.sendBadResponse(c, err.Error())
		return
	}
	resp, err := h.inMemory.Article().GetById(int(idNum))
	if err != nil {
		h.sendBadResponse(c, err.Error())
		return
	}

	c.JSON(200, resp)
}


