package handlers

import (
	"bootcamp/article/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

type handler struct {
	inMemory storage.StorageI
}

func Handler(inmemory storage.StorageI) *handler {
	return &handler{
		inMemory: inmemory,
	}
}

func (h *handler)  sendBadResponse(c *gin.Context, message string)  {
	c.JSON(http.StatusBadRequest, gin.H{
		"data": message,
	})
}
