create table if not exists authors (
   id serial primary key,
   first_name varchar(60),
   last_name varchar(60)
);