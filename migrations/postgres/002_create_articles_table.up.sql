create table if not exists articles (
    id serial primary key,
    title varchar(200) unique not null,
    body text,
    author_id int not null,
    created_at timestamp,
    updated_at timestamp,
    constraint fk_author_id foreign key(author_id) references authors(id)
);