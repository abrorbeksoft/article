
USER=user
PASSWORD=password
DATABASE=articles

start:
	go run cmd/main.go

test:
	go test test/*.go

swag-init:
		swag init -g server/server.go -o server/docs

migration:
		migrate create -ext sql -dir ./migrations/postgres -seq -digits 3 ${name}

migrate:
		migrate -path ./migrations/postgres -database 'postgres://${USER}:${PASSWORD}@localhost:5432/${DATABASE}?sslmode=disable' up

migrate-down:
			migrate -path ./migrations/postgres -database 'postgres://${USER}:${PASSWORD}@localhost:5432/${DATABASE}?sslmode=disable' down

